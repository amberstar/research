#!/bin/sh
for v in \
  ./v133-amiga-german \
  ./v173-amiga-german \
  ./v193-amiga-german \
  ./v196-amiga-english
do
  for n in A B C
  do
    b="${v}/Amber%2a_${n}"
    for x in .blkdev .bootcode .xdfmeta
    do
      [ -f  "${b}${x}" ] || continue
      rm -f "${b}${x}"
    done
    [ -d "${b}" ] || continue
    rm -rf "${b}"
  done
  [ ! "${1:-""}" = "clean" ] || continue
  for n in a b c
  do
    a="${v}/amber__${n}.adf"
    [ -f "${a}" ] || continue
    echo "${a}"
    xdftool "${a}" unpack "${v}" fsuae
    #FIXME: FS-UAE filename encoding (for special character '*')
    m="$(printf '%s' "${n}" | tr '[:lower:]' '[:upper:]')"
    if [ -d "${v}/Amber*_${m}" ]; then
      mv "${v}/Amber*_${m}"          "${v}/Amber%2a_${m}"
      mv "${v}/Amber*_${m}.blkdev"   "${v}/Amber%2a_${m}.blkdev"
      mv "${v}/Amber*_${m}.bootcode" "${v}/Amber%2a_${m}.bootcode"
      mv "${v}/Amber*_${m}.xdfmeta"  "${v}/Amber%2a_${m}.xdfmeta"
    fi
    if [ -f "${v}/Amber%2a_${m}/Amber*_Install" ]; then
      mv "${v}/Amber%2a_${m}/Amber*_Install"           "${v}/Amber%2a_${m}/Amber%2e_Install"
      mv "${v}/Amber%2a_${m}/Amber*_Install.uaem"      "${v}/Amber%2a_${m}/Amber%2e_Install.uaem"
      mv "${v}/Amber%2a_${m}/Amber*_Install.info"      "${v}/Amber%2a_${m}/Amber%2e_Install.info"
      mv "${v}/Amber%2a_${m}/Amber*_Install.info.uaem" "${v}/Amber%2a_${m}/Amber%2e_Install.info.uaem"
    fi
  done
done
